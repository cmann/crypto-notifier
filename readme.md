# Crypto Notifier

This simple node.js app is to notify users a new cryptocurrency has been listed on an exchange. This uses discord as the means of notifications. As we all can't be constantly refreshing our browsers and waiting for exchanges to let us know this app can poll at whatever interval you set.


* You must have the following environment variables set *
```
export DISCORD_BOT_TOKEN=your_token; 
export DISCORD_CHANNEL_ID=the channelID to post messages to
```
