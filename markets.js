const bittrex = require('node-bittrex-api');

exports.findNewMarkets = function (knownMarketNames) {
    return new Promise((fulfill, reject) => {
        bittrex
            .getmarkets(function (data, err) {
                if (err) {
                    reject(err)
                    return
                }
                data.markets = data.result
                data.newMarkets = []
                data
                    .markets
                    .forEach(market => {
                        const index = knownMarketNames.find(function (value) {
                            return value === market.MarketName
                        })

                        if (index === -1) {
                            data
                                .newMarkets
                                .push({logo: market.LogoUrl, name: market.MarketName, url: `https://bittrex.com/Market/Index?MarketName=${market.MarketName}`})
                        }
                    })
                fulfill(data)
            });
    })
}