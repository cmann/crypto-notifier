"use strict"

const fs = require("fs");
const jsonfile = require('jsonfile')

const file = 'markets.json'
const markets = require('./markets')

var Discord = require('discord.js')
const bot = new Discord.Client();

/**
 * You must create your own bot for this, and paste in it's token.
 */
const token = process.env.DISCORD_BOT_TOKEN

/**
 * Insert your channel id from discord.
 *
 * Can be found by enabling Developer Mode.
 *
 * User Settings ---> Appearance ---> Enable Developer Mode
 * Right Click on Channel ID
 */
const channelID = process.env.DISCORD_CHANNEL_ID

console.log(`Using Discord Bot Token: ${token}\nSending Messages to Channel: ${channelID}`)

bot.login(token)
bot.on('ready', () => {
    performCheck()
    // Set this app to never die and check every 30 seconds
    bot.setInterval(performCheck, 30 * 1000)
});
const sendMessage = function (channelID, message) {
    return new Promise((f, j) => {
        bot
            .channels
            .find("id", channelID)
            .send(message)
            .then(() => {
                console.log("Success");
                f(message)
            })
            .catch(err => {
                console.error(err)
                r(err);
            })
    })
}

// /////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////
// ////////////////////// Main Function ////////////////////////
// /////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////
const performCheck = function () {
    // find the new markets
    const previousMarkets = jsonfile.readFileSync(file)

    markets
        .findNewMarkets(previousMarkets)
        .then((data) => {
            const fileContents = JSON.stringify(data.markets.map((val) => {
                return val.MarketName
            }))

            // Write the new file
            fs.writeFile(file, fileContents, (err) => {
                if (err) {
                    console.log("error saving file");
                    console.error(err);
                }
            });
            return data
        })
        .then((data) => {
            if (data.newMarkets.length == 0) {
                console.log("No New Markets Found")
                return data
            }
            data
                .newMarkets
                .map(function (m) {
                    const message = `\n${m.logo}\n\`NEW COIN LISTING ALERT\`\n${m.name}\n${m.url}\n\n\n`
                    sendMessage(channelID, message)
                })
            return data
        })
        .catch(err => {
            console.error(err)
        })
}